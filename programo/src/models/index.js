import { Sequelize } from 'sequelize';
import config from '../config';

// eslint-disable-next-line no-unused-vars
const sequelize = new Sequelize(config.database.DB_NAME,
    config.database.DB_USER,
    config.database.DB_PASSWORD,
    {
        host: config.database.DB_HOST,
        dialect: 'mariadb',
    });

const db = {};

db.Sequelize = Sequelize;
// db.sequelize = sequelize;

export default db;
