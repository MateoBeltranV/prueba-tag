/* eslint-disable no-console */
/* eslint-disable linebreak-style */
import express from 'express';
import helmet from 'helmet';
import dotenv from 'dotenv';
import morgan from 'morgan';
import cors from 'cors';
import UsuarioSchema from './Schemas/usuario.joi';

dotenv.config();

const app = express();
app.use(express.json());
app.use(helmet());
app.use(morgan('dev'));
app.use(cors());

app.post('/usuario', async (req, res) => {
    try {
        const result = await UsuarioSchema.validateAsync(req.body);
        console.log(result);
        res.json(result);
    } catch (error) {
        res.status(400).json(error.details[0].message);
    }
});

app.listen(3000, () => {
    // eslint-disable-next-line no-console
    console.log('escuchando puerto 3000');
});
